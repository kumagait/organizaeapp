package com.kumagait.organizae.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.UpdateAttributesHandler
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunctionException
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory
import com.amazonaws.regions.Regions
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateResponse
import com.kumagait.organizae.core.StateSuccess
import com.kumagait.organizae.core.utils.error
import com.kumagait.organizae.core.utils.success
import com.kumagait.organizae.service.model.DeleteBankslipRequest
import com.kumagait.organizae.service.model.UserDTO
import com.kumagait.organizae.service.repository.BankslipService
import com.kumagait.organizae.service.repository.UserInfoService
import com.kumagait.organizae.service.repository.UserRepository
import com.kumagait.organizae.service.repository.UserService
import java.lang.Exception

class ProfileViewModel : ViewModel() {

    private val repository = UserRepository()

    private var _userDetailsLiveData = MutableLiveData<StateResponse<*>>()
    val userDetailsLiveData: LiveData<StateResponse<*>> get() = _userDetailsLiveData

    private var _deletesLiveData = MutableLiveData<StateResponse<*>>()
    val deletesLiveData: LiveData<StateResponse<*>> get() = _deletesLiveData

    private var _updateUserLiveData = MutableLiveData<StateResponse<*>>()
    val updateUserLiveData: LiveData<StateResponse<*>> get() = _updateUserLiveData

    fun getUserDetails(context: Context, userName: String) {

        val handler = object : GetDetailsHandler {
            override fun onSuccess(cognitoUserDetails: CognitoUserDetails?) {
                _userDetailsLiveData.success(cognitoUserDetails?.attributes, true)
            }

            override fun onFailure(exception: Exception?) {
                _userDetailsLiveData.error(Throwable(exception?.message), true)
            }
        }
        repository.getUserAttributes(context, userName, handler)
    }

    fun updateUserDetails(context: Context, username: String, attrName: String, attrValue: String) {
        val userAttr = CognitoUserAttributes().apply {
            addAttribute(attrName, attrValue)
        }
        val handler = object : UpdateAttributesHandler {
            override fun onSuccess(attributesVerificationList: MutableList<CognitoUserCodeDeliveryDetails>?) {
                _updateUserLiveData.success(attributesVerificationList, true)
            }

            override fun onFailure(exception: Exception?) {
                _updateUserLiveData.error(Throwable(exception?.message), true)
            }
        }
        repository.updateUserAttr(context, username, userAttr, handler)
    }

    fun deleteAccount(context: Context, username: String, deviceInfo: String) {
        val handler = object : GenericHandler {
            override fun onSuccess() {
                _deletesLiveData.postValue(StateSuccess("Sucesso"))
                removeUserData(context, UserDTO(username, deviceInfo = deviceInfo))
            }

            override fun onFailure(exception: Exception?) {
                _deletesLiveData.postValue(StateError<Throwable>(Throwable(exception?.message)))
            }
        }

        repository.deleteAccount(context, username, handler)
    }

    private fun removeUserData(context: Context, username: UserDTO) {
        val lambdaInvoker = getLambdaInterFace(context)
        val task = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<UserDTO?, Unit, Unit>() {
            override fun doInBackground(vararg params: UserDTO?) {
                try {
                    lambdaInvoker.deleteUser(params[0])
                } catch (lfe: LambdaFunctionException) { }
            }
        }
        task.execute(username)
    }

    private fun getLambdaInterFace(context: Context) : UserInfoService {
        val cognitoProvider = CognitoCachingCredentialsProvider(context.applicationContext, "us-east-1:7ea57be7-c499-43cb-a40a-0c44819aa38d", Regions.US_EAST_1)
        val factory = LambdaInvokerFactory(context.applicationContext, Regions.US_EAST_1, cognitoProvider)
        return factory.build(UserInfoService::class.java)
    }
}