package com.kumagait.organizae.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.ViewModel
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory
import com.amazonaws.regions.Regions
import com.kumagait.organizae.core.ERROR_LOG
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.service.model.*
import com.kumagait.organizae.service.repository.InsertUserService
import com.kumagait.organizae.service.repository.SignupRepository
import java.lang.Exception

class SignupViewModel : ViewModel() {

    private val repository = SignupRepository()

    private var _signupLiveData = MutableLiveData<SignupCallbackDTO>()
    val signupLiveData: LiveData<SignupCallbackDTO> get() = _signupLiveData

    private var _validationLiveData = MutableLiveData<ValidationCallback>()
    val validationLiveData: LiveData<ValidationCallback> get() = _validationLiveData

    fun signupUser(userData: UserRegisterBO, context: Context) {
        val signupHandler = object : SignUpHandler {
            override fun onSuccess(
                user: CognitoUser?,
                signUpConfirmationState: Boolean,
                cognitoUserCodeDeliveryDetails: CognitoUserCodeDeliveryDetails?
            ) {
                val state = if (signUpConfirmationState) SignupCallBackState.SUCCESS_CONFIRMED else SignupCallBackState.SUCCESS_NOT_CONFIRMED
                _signupLiveData.postValue(SignupCallbackDTO(state, user, cognitoUserCodeDeliveryDetails, null))
            }

            override fun onFailure(exception: Exception?) {
                _signupLiveData.postValue(SignupCallbackDTO(SignupCallBackState.ERROR, null, null, exception))
                Log.i(ERROR_LOG, exception?.toString())
            }
        }
        repository.registerUser(userData, context, signupHandler)
    }

    fun validateUser(username: String, validationCode: String, context: Context, deviceInfo: String) {
        val validationHandler = object : GenericHandler {
            override fun onSuccess() {
                _validationLiveData.postValue(ValidationCallback(true, null))
                insertUser(context, username, deviceInfo = deviceInfo)
            }

            override fun onFailure(exception: Exception?) {
                _validationLiveData.postValue(ValidationCallback(false, null))
                Log.i(ERROR_LOG, exception?.toString())
            }
        }

        repository.validateUserAccount(context, validationCode, username, validationHandler)
    }

    private fun insertUser(context: Context, username: String, deviceInfo: String) {
        val lambdaInterface = getLambdaInterFace(context)

        val task = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<UserDTO, Unit, UserDTO>() {
            override fun doInBackground(vararg params: UserDTO): UserDTO =
                lambdaInterface.insertUser(params[0])

            override fun onPostExecute(result: UserDTO?) {
                super.onPostExecute(result)
                if (result != null) {
                    result.clientId?.let {
                        SessionUtil.clientId = it
                    }
                }
            }
        }
        task.execute(UserDTO(username, deviceInfo = deviceInfo))
    }

    private fun getLambdaInterFace(context: Context) : InsertUserService {
        val cognitoProvider = CognitoCachingCredentialsProvider(context.applicationContext, "us-east-1:7ea57be7-c499-43cb-a40a-0c44819aa38d", Regions.US_EAST_1)
        val factory = LambdaInvokerFactory(context.applicationContext, Regions.US_EAST_1, cognitoProvider)
        return factory.build(InsertUserService::class.java)
    }

}