package com.kumagait.organizae.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kumagait.organizae.core.StateResponse
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.service.model.GenericResponse
import com.kumagait.organizae.service.model.TimelineResponse
import com.kumagait.organizae.service.model.UserDTO
import com.kumagait.organizae.service.repository.TimelineRepository

class TimelineViewModel : ViewModel() {

    private val repository = TimelineRepository()

    private var _timelineLiveData = MutableLiveData<StateResponse<TimelineResponse>>()
    val timelineLiveData: LiveData<StateResponse<TimelineResponse>> get() = _timelineLiveData

    fun getTimeline(deviceInfo: String) {
        repository.getTimeline(
            UserDTO(SessionUtil.username, deviceInfo = deviceInfo),
            _timelineLiveData
        )
    }
}