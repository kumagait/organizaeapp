package com.kumagait.organizae.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory
import com.amazonaws.regions.Regions
import com.kumagait.organizae.core.StateResponse
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.core.utils.errorData
import com.kumagait.organizae.core.utils.successData
import com.kumagait.organizae.service.model.UserDTO
import com.kumagait.organizae.service.repository.BankslipService
import com.kumagait.organizae.service.repository.InsertUserService
import com.kumagait.organizae.service.repository.LoginRepository
import com.kumagait.organizae.service.repository.LoginService
import java.lang.Exception

class LoginViewModel : ViewModel() {

    private val repository = LoginRepository()

    private var _loginLiveData = MutableLiveData<StateResponse<CognitoUserSession?>>()
    val loginLiveData: LiveData<StateResponse<CognitoUserSession?>> get() = _loginLiveData

    fun makeUserLogin(username: String, password: String, context: Context) {
        val authHandler = object : AuthenticationHandler {
            override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
                _loginLiveData.successData(userSession, true)
            }

            override fun onFailure(exception: Exception?) {
                _loginLiveData.errorData(Throwable(exception?.message), true)
            }

            override fun getAuthenticationDetails(
                authenticationContinuation: AuthenticationContinuation?,
                userId: String?
            ) {
                val authDetails = AuthenticationDetails(userId, password, null)
                authenticationContinuation?.setAuthenticationDetails(authDetails)
                authenticationContinuation?.continueTask()
            }

            override fun authenticationChallenge(continuation: ChallengeContinuation?) {
                // Not use
            }

            override fun getMFACode(continuation: MultiFactorAuthenticationContinuation?) {
                // Not use
            }
        }

        repository.logUser(context, username, authHandler)
    }
}