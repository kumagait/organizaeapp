package com.kumagait.organizae.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunctionException
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaInvokerFactory
import com.amazonaws.regions.Regions
import com.kumagait.organizae.core.StateResponse
import com.kumagait.organizae.core.utils.error
import com.kumagait.organizae.core.utils.errorData
import com.kumagait.organizae.core.utils.success
import com.kumagait.organizae.core.utils.successData
import com.kumagait.organizae.service.model.BankslipDTO
import com.kumagait.organizae.service.model.DeleteBankslipRequest
import com.kumagait.organizae.service.model.GetBankslipListRequest
import com.kumagait.organizae.service.model.GetBankslipListResponse
import com.kumagait.organizae.service.repository.BankslipService

class BankslipViewModel : ViewModel() {

    private var _insertBankslipLiveData = MutableLiveData<StateResponse<*>>()
    val insertBankslipLiveData: LiveData<StateResponse<*>> get() = _insertBankslipLiveData

    private var _bankslipListLiveData = MutableLiveData<StateResponse<GetBankslipListResponse>>()
    val bankslipListLiveData: LiveData<StateResponse<GetBankslipListResponse>> get() = _bankslipListLiveData

    private var _deleteBankslipListLiveData = MutableLiveData<StateResponse<*>>()
    val deleteBankslipListLiveData: LiveData<StateResponse<*>> get() = _deleteBankslipListLiveData

    fun insertBankslip(context: Context, DTO: BankslipDTO) {
        val lambdaInterface = getLambdaInterFace(context)

        val task = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<BankslipDTO, Unit, Unit>() {
            override fun doInBackground(vararg params: BankslipDTO?) {
                try {
                    lambdaInterface.insertBankslip(params[0])
                } catch (lfe: LambdaFunctionException) {
                    _insertBankslipLiveData.error(lfe, true)
                }
            }

            override fun onPostExecute(result: Unit?) {
                super.onPostExecute(result)
                if (result == null) {
                    _insertBankslipLiveData.error(Throwable("Error"), true)
                } else {
                    _insertBankslipLiveData.success(result, true)
                }
            }
        }
        task.execute(DTO)
    }

    fun getbankslipList(context: Context, userId: GetBankslipListRequest) {
        val lambdaInterface = getLambdaInterFace(context)

        val task = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<GetBankslipListRequest, Unit, GetBankslipListResponse>() {
            override fun doInBackground(vararg params: GetBankslipListRequest?): GetBankslipListResponse {
                return lambdaInterface.getBankslipList(params[0])
            }

            override fun onPostExecute(result: GetBankslipListResponse?) {
                super.onPostExecute(result)
                if (result == null) {
                    _bankslipListLiveData.errorData(Throwable("Error"), true)
                } else {
                    _bankslipListLiveData.successData(result, true)
                }
            }
        }
        task.execute(userId)
    }

    fun deleteBankslip(context: Context, request: DeleteBankslipRequest?) {
        val lambdaInterface = getLambdaInterFace(context)

        val task = @SuppressLint("StaticFieldLeak")
        object : AsyncTask<DeleteBankslipRequest?, Unit, Unit>() {
            override fun doInBackground(vararg params: DeleteBankslipRequest?) {
                try {
                    lambdaInterface.removeBankslip(params[0])
                } catch (lfe: LambdaFunctionException) {
                    _deleteBankslipListLiveData.error(lfe, true)
                }
            }

            override fun onPostExecute(result: Unit?) {
                super.onPostExecute(result)
                if (result == null) {
                    _deleteBankslipListLiveData.error(Throwable("Error"), true)
                } else {
                    _deleteBankslipListLiveData.success(result, true)
                }
            }
        }
        task.execute(request)
    }

    private fun getLambdaInterFace(context: Context) : BankslipService{
        val cognitoProvider = CognitoCachingCredentialsProvider(context.applicationContext, "us-east-1:7ea57be7-c499-43cb-a40a-0c44819aa38d", Regions.US_EAST_1)
        val factory = LambdaInvokerFactory(context.applicationContext, Regions.US_EAST_1, cognitoProvider)
        return factory.build(BankslipService::class.java)
    }
}