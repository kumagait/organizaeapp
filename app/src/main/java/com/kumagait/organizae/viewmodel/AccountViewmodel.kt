package com.kumagait.organizae.viewmodel

import android.se.omapi.Session
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kumagait.organizae.core.StateResponse
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.service.model.*
import com.kumagait.organizae.service.repository.AccountRepository

class AccountViewmodel : ViewModel() {

    private val repository = AccountRepository()

    private var _createAccountLiveData = MutableLiveData<StateResponse<*>>()
    val createAccountLiveData: LiveData<StateResponse<*>> get() = _createAccountLiveData

    private var _accountListLiveData = MutableLiveData<StateResponse<GetAccountListResponse>>()
    val accountListLiveData: LiveData<StateResponse<GetAccountListResponse>> get() = _accountListLiveData

    private var _paymentLiveData = MutableLiveData<StateResponse<*>>()
    val paymentLiveData: LiveData<StateResponse<*>> get() = _paymentLiveData

    private var _updateLiveData = MutableLiveData<StateResponse<*>>()
    val updateLiveData: LiveData<StateResponse<*>> get() = _updateLiveData

    private var _deleteLiveData = MutableLiveData<StateResponse<GenericResponse>>()
    val deleteLiveData: LiveData<StateResponse<GenericResponse>> get() = _deleteLiveData

    fun createAccount(accountName: String, deviceInfo: String) {
        repository.createAccount(
            CreateAccountRequestDTO(
                SessionUtil.username,
                accountName,
                deviceInfo
            ),
            _createAccountLiveData
        )
    }

    fun updateAccount(accountId: String, value: Float, transactionType: String) {
        repository.updateAccount(
            UpdateAccountRequest(accountId, value, transactionType),
            _updateLiveData
        )
    }

    fun performPayment(accountId: String, bankslipId: String?, deviceInfo: String) {
        repository.performPayment(
            PerformPaymentRequest(accountId, bankslipId, deviceInfo),
            _paymentLiveData
        )
    }

    fun getAccountList(deviceInfo: String) {
        repository.getAccountList(
            UserDTO(SessionUtil.username, deviceInfo = deviceInfo),
            _accountListLiveData
        )
    }

    fun getAvaliableAccountsForBankslip(bankslipId: String?, deviceInfo: String) {
        repository.getAvaliableAccountsForBankslip(
            GetAvaliableAccountsRequest(SessionUtil.username, bankslipId, deviceInfo),
            _accountListLiveData
        )
    }

    fun deleteAccount(accountId: String, deviceInfo: String) {
        repository.deleteAccount(
            DeleteAccountRequest(accountId, deviceInfo),
            _deleteLiveData
        )
    }

}