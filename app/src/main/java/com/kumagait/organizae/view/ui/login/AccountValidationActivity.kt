package com.kumagait.organizae.view.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.method.DigitsKeyListener
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.kumagait.organizae.R
import com.kumagait.organizae.core.getDeviceSettings
import com.kumagait.organizae.core.utils.SimpleTextWatcher
import com.kumagait.organizae.view.ui.MainActivity
import com.kumagait.organizae.view.ui.register.BaseRegisterActivity
import com.kumagait.organizae.view.ui.register.RegisterIntent.getUserData
import com.kumagait.organizae.view.ui.register.RegisterRouter
import com.kumagait.organizae.viewmodel.SignupViewModel
import kotlinx.android.synthetic.main.activity_base_register.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.newTask
import org.jetbrains.anko.toast

class AccountValidationActivity: BaseRegisterActivity(), RegisterRouter {

    private val viewModel: SignupViewModel by viewModels()
    private var isUpdateAccount = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_register)
        isUpdateAccount = intent.getBooleanExtra(IS_FROM_UPDATE_KEY, false)
        observeResponse()
        userData = getUserData()
        initViews()
    }

    private fun initViews() {
        val codeValidator = object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let { toggleGradientButton(s.isNotEmpty() && s.length == 6) }
            }
        }

        initRegisterViews(
            R.string.tv_label_birthday,
            R.string.et_hint_account_validation,
            InputType.TYPE_CLASS_NUMBER,
            textListener = codeValidator,
            keyListener = DigitsKeyListener.getInstance("0123456789")
        )

        tvSignupLabel.text = getString(R.string.tv_label_account_validation, userData.email)
        btnBack.run {
            visibility = View.INVISIBLE
            isEnabled = false
        }
        tvSignuptitle.text = getString(R.string.tv_title_validation)

        btnRegisterNext.setOnClickListener {
            viewModel.validateUser(userData.username, etSignup.text.toString(), this, getDeviceSettings())
        }
    }

    private fun observeResponse() {
        viewModel.validationLiveData.observe(this, Observer { response ->
            if (response.isValidaCode) {
                toast("Sucesso")
                if (isUpdateAccount) {
                    startActivity(Intent(this, MainActivity::class.java).newTask().clearTask())
                } else {
                    finish()
                }
            } else {
                toast(response.error?.message.toString())
            }
        })
    }

    companion object {
        const val IS_FROM_UPDATE_KEY = "isFromUpdateKey"
    }
}