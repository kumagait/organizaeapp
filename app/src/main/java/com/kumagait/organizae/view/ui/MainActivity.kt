package com.kumagait.organizae.view.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kumagait.organizae.R
import com.kumagait.organizae.view.ui.login.LoginActivity
import com.kumagait.organizae.view.ui.register.RegisterNicknameActivity
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.intentFor

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRegister.setOnClickListener { view ->
            startActivity(intentFor<RegisterNicknameActivity>())
        }

        btnLogin.setOnClickListener {
            startActivity(intentFor<LoginActivity>())
        }
    }

}
