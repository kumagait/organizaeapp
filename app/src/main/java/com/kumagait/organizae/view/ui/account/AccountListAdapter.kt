package com.kumagait.organizae.view.ui.account

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kumagait.organizae.R
import com.kumagait.organizae.service.model.AccountDTO
import kotlinx.android.synthetic.main.account_view_item.view.*

class AccountListAdapter(private val accountList: List<AccountDTO>, private val listener: AccountClickListener) :
    RecyclerView.Adapter<AccountListAdapter.AccountViewHolder>() {

    class AccountViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): AccountViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.account_view_item, parent, false)
        return AccountViewHolder(
            view
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        holder.view.tvAccountName.text = accountList[position].accountName
        holder.view.tvBalanceValue.text = "R$ ${accountList[position].accountBalance}".replace(".", ",")

        holder.view.rootView.setOnClickListener {
            listener.onAccountClick(accountList[position])
        }
    }

    override fun getItemCount() = accountList.size
}