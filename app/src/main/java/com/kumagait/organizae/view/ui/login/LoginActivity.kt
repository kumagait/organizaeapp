package com.kumagait.organizae.view.ui.login

import android.os.Bundle
import android.text.InputType
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.kumagait.organizae.R
import com.kumagait.organizae.core.*
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.core.utils.SimpleTextWatcher
import com.kumagait.organizae.view.ui.home.HomeActivity
import com.kumagait.organizae.view.ui.home.HomeActivity.Companion.KEY_USER_ID
import com.kumagait.organizae.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

class LoginActivity : AppCompatActivity() {

    private val viewModel by viewModels<LoginViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColor(R.color.white_FFFFFF, true)
        setContentView(R.layout.activity_login)
        configureView()
    }

    private fun configureView() {
        btnBack.setOnClickListener { onBackPressed() }

        btnLogin.run {
            isEnabled = false
            setOnClickListener {
                val username = etUsername.text.toString()
                val password = etPassword.text.toString()
                viewModel.makeUserLogin(username, password, this@LoginActivity)
            }
        }

        etUsername.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyButtonEnabledAndRemoveError()
            }
        })

        etPassword.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyButtonEnabledAndRemoveError()
            }
        })

        etPassword.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        viewModel.loginLiveData.observe(this, Observer { response ->
            when (response) {
                is StateSuccess -> {
                    SessionUtil.username = response.data!!.username
                    SessionUtil.deviceKey = CognitoSettings(this).getUserPool().getUser(SessionUtil.username).thisDevice().deviceKey
                    startActivity(intentFor<HomeActivity>().putExtra(KEY_USER_ID, response.data.username).clearTask().newTask())
                }
                is StateError -> {
                    tvLoginError.visibility = View.VISIBLE
                }
            }

        })
    }

    private fun verifyButtonEnabledAndRemoveError() {
        tvLoginError.visibility = View.GONE
        btnLogin.isEnabled = etUsername.text.isNotEmpty() && etPassword.text.length >= 8
    }

}