package com.kumagait.organizae.view.ui.timeline

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kumagait.organizae.R
import com.kumagait.organizae.core.utils.formatToDate
import com.kumagait.organizae.service.model.TimelineItem
import com.kumagait.organizae.view.ui.insertbankslip.BankslipClickListener
import kotlinx.android.synthetic.main.timeline_view_item.view.*
import java.text.SimpleDateFormat

class TimelineAdapter : RecyclerView.Adapter<TimelineAdapter.TimelineViewHolder>() {

    private var timelineList: List<TimelineItem> = listOf()

    class TimelineViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): TimelineViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.timeline_view_item, parent, false)
        return TimelineViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TimelineViewHolder, position: Int) {
        with(holder.view) {
            tvTransactionTypeValue.text = when(timelineList[position].transactionType) {
                PAYMENT -> OPERATIONS[0]
                CASHIN -> OPERATIONS[1]
                CASHOUT -> OPERATIONS[2]
                else -> "Não definido"
            }

            tvAccountName.text = timelineList[position].accountName

            if (timelineList[position].transactionType == PAYMENT) {
                tvBankslipName.visibility = View.VISIBLE
                tvBankslipNameValue.visibility = View.VISIBLE
                tvBankslipNameValue.text = timelineList[position].bankslipName
            } else {
                tvBankslipName.visibility = View.GONE
                tvBankslipNameValue.visibility = View.GONE
            }

            tvTransactionAmountValue.text = "R$ ${timelineList[position].transactionValue}".replace(".", ",")
            tvTransactionDateValue.text = timelineList[position].transactionDate.formatToDate()
        }
    }

    override fun getItemCount() = timelineList.size

    fun updateData(dataSet: List<TimelineItem>) {
        timelineList = dataSet.sortedBy { SimpleDateFormat("yyyy-MM-dd").parse(it.transactionDate).time }
        notifyDataSetChanged()
    }

    companion object {
        val OPERATIONS = listOf("Pagamento de boleto", "Deposito", "Saque")
        const val PAYMENT = "payment"
        const val CASHIN = "cashin"
        const val CASHOUT = "cashout"
    }
}