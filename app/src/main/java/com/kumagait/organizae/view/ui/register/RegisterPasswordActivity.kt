package com.kumagait.organizae.view.ui.register

import android.os.Bundle
import android.text.InputType
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kumagait.organizae.R
import com.kumagait.organizae.core.utils.SimpleTextWatcher
import com.kumagait.organizae.service.model.SignupCallBackState
import com.kumagait.organizae.viewmodel.SignupViewModel
import kotlinx.android.synthetic.main.activity_base_register.*
import org.jetbrains.anko.toast

class RegisterPasswordActivity : BaseRegisterActivity(), RegisterRouter {

    private val viewModel: SignupViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_register)
        userData = RegisterIntent.getUserData()
        observeSigupResponse()
        initViews()
    }

    private fun initViews() {
        val passwordValidator = object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let { toggleGradientButton(s.isNotEmpty() && s.length >= 8) }
            }
        }

        initRegisterViews(
            R.string.tv_label_password,
            R.string.et_hint_password,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD,
            textListener = passwordValidator
        )

        btnRegisterNext.setOnClickListener {
            userData.password = etSignup.text.toString()
            viewModel.signupUser(userData, this)
        }
    }

    private fun observeSigupResponse() {
        viewModel.signupLiveData.observe(this, Observer {
            if (it.state != SignupCallBackState.ERROR)
                loadValidationActivity(userData)
            else toast("Erro sistemico tente novamente")

        })
    }
}