package com.kumagait.organizae.view.ui.register

import android.os.Bundle
import android.text.InputType
import com.kumagait.organizae.R
import com.kumagait.organizae.core.EMAIL_REGEX
import com.kumagait.organizae.core.utils.SimpleTextWatcher
import kotlinx.android.synthetic.main.activity_base_register.*
import java.util.regex.Pattern

class RegisterEmailActivity : BaseRegisterActivity(), RegisterRouter {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_register)
        userData = RegisterIntent.getUserData()
        initViews()
    }

    private fun initViews() {
        val nameValidator = object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputErrorVisibility(false)
                s?.let {
                    toggleGradientButton(it.toString().isValidEmail())
                }
            }
        }

        initRegisterViews(
            R.string.tv_label_email,
            R.string.et_hint_email,
            InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS,
            textListener = nameValidator
        )

        btnRegisterNext.setOnClickListener {
            userData.email = etSignup.text.toString()
            loadUsernameActivity(userData)
        }
    }

    private fun String.isValidEmail(): Boolean =
        this.isNotEmpty() && Pattern.compile(EMAIL_REGEX).matcher(this).matches()
}