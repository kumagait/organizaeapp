package com.kumagait.organizae.view.ui.account

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.kumagait.organizae.R
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateSuccess
import com.kumagait.organizae.core.getDeviceSettings
import com.kumagait.organizae.service.model.AccountDTO
import com.kumagait.organizae.view.ui.account.AccountActivity.Companion.KEY_ACCOUNT
import com.kumagait.organizae.viewmodel.AccountViewmodel
import kotlinx.android.synthetic.main.activity_account_details.*
import org.jetbrains.anko.toast

class AccountDetailActivity : AppCompatActivity() {

    private val viewModel by viewModels<AccountViewmodel>()
    private var account: AccountDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_details)
        observeUpdateAccount()
        observeDeleteAccount()
        account = intent.extras?.getParcelable(KEY_ACCOUNT)
        configureViews()
    }

    @SuppressLint("SetTextI18n")
    private fun configureViews() {
        account?.let {
            tvAccountName.text = it.accountName
            tvBankslipValue.text = "R$ ${it.accountBalance.toString().replace(".", ",")}"
        }

        btnCashin.setOnClickListener {
            account?.let {
                viewModel.updateAccount(
                    it.accountId,
                    getUpdateValue(),
                    CASHIN
                )
            }
        }

        btnCashout.setOnClickListener {
            account?.let {
                viewModel.updateAccount(
                    it.accountId,
                    getUpdateValue(),
                    CASHOUT
                )
            }
        }

        btnBack.setOnClickListener {
            onBackPressed()
        }

        btnDeleteAccount.setOnClickListener {
            account?.let {
                viewModel.deleteAccount(it.accountId, getDeviceSettings())
            }
        }
    }

    private fun getUpdateValue(): Float = etBalance.text.toString().toFloat()

    private fun observeUpdateAccount() {
        viewModel.updateLiveData.observe(this, Observer { response ->
            when(response) {
                is StateSuccess -> {
                    toast("Saldo atualizado")
                    finish()
                }
                is StateError -> toast("Erro na aplicação")
            }
        })
    }

    private fun observeDeleteAccount() {
        viewModel.deleteLiveData.observe(this, Observer { response ->
            when(response) {
                is StateSuccess -> {
                    when(response.data.statusCode) {
                        200 -> { finish() }
                        403 -> { showAccountBalanceNotZero() }
                    }
                }
                is StateError -> toast("Erro na aplicação")
            }
        })
    }

    private fun showAccountBalanceNotZero() {
        AlertDialog.Builder(this)
            .setTitle("A conta não pode ser deletada!")
            .setMessage("Para deletar uma conta é necessário que o saldo esteja zerado!")
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setNegativeButton(R.string.dialog_ok, null).show()
    }

    companion object {
        private const val CASHIN = "cashin"
        private const val CASHOUT = "cashout"
    }

}