package com.kumagait.organizae.view.ui.profile

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.kumagait.organizae.R
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateSuccess
import com.kumagait.organizae.core.getDeviceSettings
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.service.model.UserRegisterBO
import com.kumagait.organizae.view.ui.MainActivity
import com.kumagait.organizae.view.ui.register.RegisterRouter
import com.kumagait.organizae.view.ui.register.loadValidationActivity
import com.kumagait.organizae.viewmodel.ProfileViewModel
import kotlinx.android.synthetic.main.activity_user_profile.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.newTask
import org.jetbrains.anko.toast

class UserProfileActivity : AppCompatActivity(), RegisterRouter {

    private val viewModel by viewModels<ProfileViewModel>()
    private var isEmailUpdate = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)
        configureView()
        observeAccountDetailsResponse()
        observeDeleteResponse()
        observeDataUpdate()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUserDetails(this, SessionUtil.username)
    }

    private fun configureView() {
        btnBack.setOnClickListener { onBackPressed() }

        btnExit.setOnClickListener {
            exitApp()
        }

        btnDelete.setOnClickListener {
            showDeleteAccountDialog()
        }

        btnUpdateEmail.setOnClickListener {
            isEmailUpdate = true
            viewModel.updateUserDetails(
                this,
                SessionUtil.username,
                "email",
                etUserEmail.text.toString()
            )
        }

        btnUpdateNickname.setOnClickListener {
            viewModel.updateUserDetails(
                this,
                SessionUtil.username,
                "nickname",
                etUserNickName.text.toString()
            )
        }
    }

    private fun observeAccountDetailsResponse() {
        viewModel.userDetailsLiveData.observe(this, Observer { response ->
            when(response) {
                is StateSuccess -> {
                    with((response.data as CognitoUserAttributes).attributes) {
                        tvUsername.text = SessionUtil.username
                        tvUserBirthday.text = this["birthdate"]
                        etUserEmail.setText(this["email"])
                        etUserNickName.setText(this["nickname"])
                    }
                }
                is StateError -> toast("Erro na aplicação")
            }
        })
    }

    private fun observeDeleteResponse() {
        viewModel.deletesLiveData.observe(this, Observer {  response ->
            when(response) {
                is StateSuccess -> exitApp()
                is StateError -> toast("Erro na aplicação")
            }
        })
    }

    private fun observeDataUpdate() {
        viewModel.updateUserLiveData.observe(this, Observer {  response ->
            when(response) {
                is StateSuccess -> {
                    if (isEmailUpdate) {
                        loadValidationActivity(UserRegisterBO(username = SessionUtil.username), true)
                    }
                    isEmailUpdate = false
                    toast("Alterado com sucesso")
                }
            }
        })
    }

    private fun showDeleteAccountDialog() {
        AlertDialog.Builder(this)
            .setTitle("Deseja deletar a conta?")
            .setMessage("Esta operação é irreversivel deseja mesmo deletar?")
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setPositiveButton(R.string.dialog_yes
            ) { _, _ ->
                viewModel.deleteAccount(this@UserProfileActivity, SessionUtil.username, getDeviceSettings())
            }
            .setNegativeButton(R.string.dialog_no, null).show()
    }

    private fun exitApp() =
        startActivity(Intent(this, MainActivity::class.java).clearTask().newTask())

}