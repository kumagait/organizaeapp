package com.kumagait.organizae.view.ui.register

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kumagait.organizae.core.USER_DATA
import com.kumagait.organizae.service.model.UserRegisterBO
import com.kumagait.organizae.view.ui.login.AccountValidationActivity
import com.kumagait.organizae.view.ui.login.AccountValidationActivity.Companion.IS_FROM_UPDATE_KEY
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.newTask
import java.lang.UnsupportedOperationException

object RegisterIntent {
    val intent = Intent()
    val bundle = Bundle()

    fun getUserData() : UserRegisterBO {
        if (intent.extras != null)
            return intent.extras!!.getParcelable(USER_DATA)!!
        return UserRegisterBO()
    }
}

interface RegisterRouter

fun RegisterRouter.loadBirthdayActivity(user: UserRegisterBO) {
    activity().startActivity(createIntent(user, RegisterBirthDayActivity::class.java))
}

fun RegisterRouter.loadEmailActivity(user: UserRegisterBO) {
    activity().startActivity(createIntent(user, RegisterEmailActivity::class.java))
}

fun RegisterRouter.loadUsernameActivity(user: UserRegisterBO) {
    activity().startActivity(createIntent(user, RegisterUsernameActivity::class.java))
}

fun RegisterRouter.loadPasswordActivity(user: UserRegisterBO) {
    activity().startActivity(createIntent(user, RegisterPasswordActivity::class.java))
}

fun RegisterRouter.loadValidationActivity(user: UserRegisterBO, isEmailUpdate: Boolean = false) {
    activity().startActivity(
        createIntent(user, AccountValidationActivity::class.java)
            .putExtra(IS_FROM_UPDATE_KEY, isEmailUpdate)
            .newTask()
            .clearTask()
    )
}

private fun RegisterRouter.activity(): AppCompatActivity {
    if (this is AppCompatActivity) {
        return this
    } else {
        throw UnsupportedOperationException("This activity must be a AppCompatActivity")
    }
}

private fun RegisterRouter.createIntent(
    user: UserRegisterBO?,
    kClass: Class<*>
): Intent {
    RegisterIntent.intent.component = ComponentName(activity(), kClass)
    RegisterIntent.intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
    if (user != null) {
        RegisterIntent.bundle.putParcelable(USER_DATA, user)
        RegisterIntent.intent.putExtras(RegisterIntent.bundle)
    }

    return RegisterIntent.intent
}