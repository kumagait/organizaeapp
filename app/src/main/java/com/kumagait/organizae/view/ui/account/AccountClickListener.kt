package com.kumagait.organizae.view.ui.account

import com.kumagait.organizae.service.model.AccountDTO

interface AccountClickListener {

    fun onAccountClick(account:AccountDTO)
}