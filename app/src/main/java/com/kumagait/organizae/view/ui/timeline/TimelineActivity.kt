package com.kumagait.organizae.view.ui.timeline

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kumagait.organizae.R
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateSuccess
import com.kumagait.organizae.core.getDeviceSettings
import com.kumagait.organizae.service.model.TimelineItem
import com.kumagait.organizae.service.model.TimelineResponse
import com.kumagait.organizae.view.ui.timeline.TimelineAdapter.Companion.OPERATIONS
import com.kumagait.organizae.viewmodel.TimelineViewModel
import kotlinx.android.synthetic.main.activity_timeline.*
import org.jetbrains.anko.sdk27.coroutines.onItemSelectedListener
import org.jetbrains.anko.toast
import java.sql.Time

class TimelineActivity : AppCompatActivity() {

    private val viewModel by viewModels<TimelineViewModel>()
    private lateinit var timelineResponse: TimelineResponse
    private lateinit var timeline: List<TimelineItem>
    private lateinit var filteredTimeline: List<TimelineItem>
    private lateinit var accountList: List<String>
    private val timelineAdapter = TimelineAdapter()
    private var isUserInteracting = false

    private var accountFilter = ""
    private var operationFilter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timeline)
        createViews()
        observeTimelineLiveData()
        viewModel.getTimeline(getDeviceSettings())
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        isUserInteracting = true
    }

    private fun createViews() {
        btnBack.setOnClickListener { onBackPressed() }

        rvTimeline.run {
            layoutManager = LinearLayoutManager(this@TimelineActivity)
            adapter = timelineAdapter
        }
    }

    private fun observeTimelineLiveData() {
        viewModel.timelineLiveData.observe(this, Observer { response ->
            when(response) {
                is StateSuccess -> {
                    timeline = response.data.timeline
                    accountList = response.data.accountList
                    timelineAdapter.updateData(timeline)
                    setupViews()
                }
                is StateError -> {
                    toast("Erro na aplicação")
                    finish()
                }
            }
        })
    }

    private fun setupViews() {
        val accountAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
            this@TimelineActivity,
            android.R.layout.simple_spinner_dropdown_item,
            listOf("") + accountList
        )
        spAccountFilter.run {
            adapter = accountAdapter
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                    if (isUserInteracting) {
                        accountFilter = parent.getItemAtPosition(pos) as String
                        timelineAdapter.updateData(filterList())
                    }
                }
                override fun onNothingSelected(parent: AdapterView<*>) {}
            }
        }
        val operationAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
            this@TimelineActivity,
            android.R.layout.simple_spinner_dropdown_item,
            listOf("") + OPERATIONS
        )
        spOperationFilter.run {
            adapter = operationAdapter
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                    if (isUserInteracting) {
                        operationFilter = parent.getItemAtPosition(pos) as String
                        timelineAdapter.updateData(filterList())
                    }
                }
                override fun onNothingSelected(parent: AdapterView<*>) {}
            }
        }
    }

    private fun filterList(): List<TimelineItem> {
        val operationFilterIsEnabled = operationFilter.isNotEmpty()
        val accountFilterIsEnabled = accountFilter.isNotEmpty()

        return when {
            operationFilterIsEnabled && !accountFilterIsEnabled -> timeline.filter { getTransactionType(it.transactionType) == operationFilter }
            !operationFilterIsEnabled && accountFilterIsEnabled -> timeline.filter { it.accountName == accountFilter }
            operationFilterIsEnabled && accountFilterIsEnabled -> timeline.filter { (it.accountName == accountFilter) && (getTransactionType(it.transactionType) == operationFilter) }
            else -> timeline
        }
    }

    private fun getTransactionType(type: String): String {
        return when(type) {
            TimelineAdapter.PAYMENT -> OPERATIONS[0]
            TimelineAdapter.CASHIN -> OPERATIONS[1]
            TimelineAdapter.CASHOUT -> OPERATIONS[2]
            else -> "Não definido"
        }
    }
}