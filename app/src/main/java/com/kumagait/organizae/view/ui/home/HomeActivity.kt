package com.kumagait.organizae.view.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.kumagait.organizae.R
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateSuccess
import com.kumagait.organizae.core.getDeviceSettings
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.service.model.BankslipDTO
import com.kumagait.organizae.service.model.BankslipStatus
import com.kumagait.organizae.service.model.GetBankslipListRequest
import com.kumagait.organizae.view.ui.account.AccountActivity
import com.kumagait.organizae.view.ui.bankslipdetail.BankslipDetailActivity
import com.kumagait.organizae.view.ui.bankslipdetail.BankslipDetailActivity.Companion.KEY_BANKSLIP_DETAIL
import com.kumagait.organizae.view.ui.insertbankslip.BankslipClickListener
import com.kumagait.organizae.view.ui.insertbankslip.InsertBankslipActivity
import com.kumagait.organizae.view.ui.profile.UserProfileActivity
import com.kumagait.organizae.view.ui.timeline.TimelineActivity
import com.kumagait.organizae.viewmodel.BankslipViewModel
import com.kumagait.organizae.viewmodel.ProfileViewModel
import kotlinx.android.synthetic.main.activity_home.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class HomeActivity : AppCompatActivity(), BankslipClickListener {

    private val viewModel by viewModels<BankslipViewModel>()
    private val profileViewModel by viewModels<ProfileViewModel>()
    private var userId = SessionUtil.username

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setupViews()
        observeBankslipListResponse()
        observeGetProfile()
        userId = intent.getStringExtra(KEY_USER_ID) ?: SessionUtil.username
    }

    override fun onResume() {
        super.onResume()
        profileViewModel.getUserDetails(this, userId)
    }

    private fun setupViews() {
        btnAddBankslip.setOnClickListener {
            startActivity<InsertBankslipActivity>()
        }

        btnSeeAccounts.setOnClickListener {
            startActivity<AccountActivity>()
        }

        tvSeeProfile.setOnClickListener {
            startActivity<UserProfileActivity>()
        }

        btnTimeline.setOnClickListener {
            startActivity<TimelineActivity>()
        }
    }

    private fun observeGetProfile() {
        profileViewModel.userDetailsLiveData.observe(this, Observer {
            if (it is StateSuccess) {
                with((it.data as CognitoUserAttributes).attributes) {
                    tvHelloUser.visibility = View.VISIBLE
                    tvHelloUser.text = "Olá ${this["nickname"]}"
                    SessionUtil.clientSub = this["sub"] ?: "Sub not found"
                    viewModel.getbankslipList(this@HomeActivity, GetBankslipListRequest(userId, getDeviceSettings()))
                }
            }
        })
    }

    private fun observeBankslipListResponse() {
        viewModel.bankslipListLiveData.observe(this, Observer { response ->
            when (response) {
                is StateSuccess -> {
                    if (response.data.bankslipList.isNotEmpty()) {
                        tvEmptyList.visibility = View.GONE
                        rvbankslipList.run {
                            layoutManager = LinearLayoutManager(this@HomeActivity)
                            adapter =
                                BankslipListAdapter(
                                    response.data.bankslipList.sort(),
                                    this@HomeActivity
                                )
                        }
                    } else {
                        tvEmptyList.visibility = View.VISIBLE
                    }
                }
                is StateError -> {
                    toast("Erro na aplicação")
                }
            }
        })
    }

    private fun List<BankslipDTO>.sort() =
        this.sortedBy { SimpleDateFormat("yyyy-MM-dd").parse(it.dueDate).time }
            .sortedBy { it.status }

    companion object {
        const val KEY_USER_ID = "keyUserId"
    }

    override fun performClick(bankslipInfo: BankslipDTO) {
        val bundle = Bundle().apply { putParcelable(KEY_BANKSLIP_DETAIL, bankslipInfo) }
        startActivity(Intent(this, BankslipDetailActivity::class.java).putExtras(bundle))
    }
}