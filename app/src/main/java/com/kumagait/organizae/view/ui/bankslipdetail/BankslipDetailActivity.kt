package com.kumagait.organizae.view.ui.bankslipdetail

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.kumagait.organizae.R
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateSuccess
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.core.utils.formatToDate
import com.kumagait.organizae.service.model.BankslipDTO
import com.kumagait.organizae.service.model.BankslipStatus
import com.kumagait.organizae.service.model.DeleteBankslipRequest
import com.kumagait.organizae.view.ui.account.AccountActivity
import com.kumagait.organizae.viewmodel.BankslipViewModel
import kotlinx.android.synthetic.main.activity_bankslip_detail.*
import org.jetbrains.anko.toast


class BankslipDetailActivity : AppCompatActivity() {

    private val viewModel by viewModels<BankslipViewModel>()
    private var bankslipInfo: BankslipDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bankslip_detail)
        setupViews()
        observeDeleteResponse()
    }

    private fun observeDeleteResponse() {
        viewModel.deleteBankslipListLiveData.observe(this, androidx.lifecycle.Observer { response ->
            when (response) {
                is StateSuccess -> finish()
                is StateError -> toast("Erro na aplicação")
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun setupViews() {
       bankslipInfo = intent.extras?.getParcelable(KEY_BANKSLIP_DETAIL)
        btnBack.setOnClickListener { onBackPressed() }

        bankslipInfo?.let {
            if (it.status == BankslipStatus.PAID)
                btnPay.visibility = View.GONE
            tvBankslipTitle.text = it.bankslipName
            tvDueDate.text = it.dueDate.formatToDate()
            tvBarcodeValue.text = it.bankslipCode
            tvBankslipValue.text = "R$ ${it.bankslipValue}"
        }

        btnDelete.setOnClickListener {
            showConfirmationDialog()
        }

        btnPay.setOnClickListener {
            val bundle = Bundle().apply {
                putParcelable(KEY_BANKSLIP_DETAIL, bankslipInfo)
            }
            startActivity(Intent(this, AccountActivity::class.java).putExtras(bundle))
        }
    }

    private fun showConfirmationDialog() {
        AlertDialog.Builder(this)
            .setTitle("Deletar ${bankslipInfo?.bankslipName}")
            .setMessage("Esta operação é irreversivel deseja mesmo deletar?")
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setPositiveButton(R.string.dialog_yes
            ) { _, _ ->
                viewModel.deleteBankslip(this@BankslipDetailActivity, generateDeleteObject())
            }
            .setNegativeButton(R.string.dialog_no, null).show()
    }

    private fun generateDeleteObject() = bankslipInfo?.bankslipId?.let {
        DeleteBankslipRequest(SessionUtil.username, it)
    }
    companion object {
        const val KEY_BANKSLIP_DETAIL = "keyBankslipDetail"
    }

}