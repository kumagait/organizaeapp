package com.kumagait.organizae.view.ui.insertbankslip

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.integration.android.IntentIntegrator
import com.kumagait.organizae.R
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateSuccess
import com.kumagait.organizae.core.getDeviceSettings
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.core.utils.SimpleTextWatcher
import com.kumagait.organizae.service.model.BankslipDTO
import com.kumagait.organizae.service.model.BankslipStatus
import com.kumagait.organizae.viewmodel.BankslipViewModel
import kotlinx.android.synthetic.main.activity_insert_bankslip.*
import org.jetbrains.anko.toast
import java.util.*


class InsertBankslipActivity : AppCompatActivity() {

    private val viewModel by viewModels<BankslipViewModel>()
    private val calendar = Calendar.getInstance()
    private var dataWasSelected = false
    private var dueDate = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert_bankslip)
        setupViews()
        observeBankslipResponse()
    }

    override fun onResume() {
        super.onResume()
        verifyFields()
    }

    private fun setupViews() {
        btnBack.setOnClickListener {
            onBackPressed()
        }

        cvDueDate.run {
            minDate = calendar.timeInMillis
            setOnDateChangeListener { view, year, month, dayOfMonth ->
                dueDate = "$year-${month + 1}-$dayOfMonth"
                dataWasSelected = true
                verifyFields()
            }
        }

        btnInsertBankslip.setOnClickListener {
            viewModel.insertBankslip(this, createRequestObject())
        }

        etBankslipCode.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyFields()
            }
        })

        etBankslipName.run {
            addTextChangedListener(object : SimpleTextWatcher() {
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    verifyFields()
                }
            })
        }

        etBankslipValue.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                verifyFields()
            }
        })

        btnBankslipCamera.setOnClickListener {
            IntentIntegrator(this).apply {
                setPrompt("Escaneie um código de barras")
                setCameraId(0)
                setBeepEnabled(false)
                setBarcodeImageEnabled(true)
            }.initiateScan()
        }
    }

    private fun observeBankslipResponse() {
        viewModel.insertBankslipLiveData.observe(this, androidx.lifecycle.Observer { response ->
            when (response) {
                is StateSuccess -> {
                    toast("Adicionado com sucesso")
                    etBankslipName.setText("")
                    etBankslipCode.setText("")
                    etBankslipValue.setText("")
                }
                is StateError -> toast("Erro na aplicação")
            }
        })
    }

    private fun createRequestObject(): BankslipDTO {
        val bankslipName = etBankslipName.text.toString()
        val bankslipCode = etBankslipCode.text.toString()
        val bankslipValue = etBankslipValue.text.toString().toFloat()

        return BankslipDTO(
            bankslipName = bankslipName,
            bankslipCode = bankslipCode,
            bankslipValue = bankslipValue,
            status = BankslipStatus.OPEN,
            dueDate = dueDate,
            username = SessionUtil.username,
            deviceInfo = getDeviceSettings()
        )
    }

    private fun verifyFields() {
        val bankslipNameIsFilled = etBankslipName.text.toString().isNotEmpty()
        val bankslipCodeIsFilled = etBankslipCode.text.toString().isNotEmpty()
        val bankslipValueIsFilled = etBankslipValue.text.toString().isNotEmpty()

        btnInsertBankslip.isEnabled =
            bankslipCodeIsFilled && bankslipNameIsFilled && bankslipValueIsFilled && dataWasSelected
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents != null) {
                etBankslipCode.setText(result.contents)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}