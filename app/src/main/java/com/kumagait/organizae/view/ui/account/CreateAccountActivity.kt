package com.kumagait.organizae.view.ui.account

import android.os.Bundle
import android.text.Editable
import android.text.method.DigitsKeyListener
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.kumagait.organizae.R
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateSuccess
import com.kumagait.organizae.core.getDeviceSettings
import com.kumagait.organizae.core.utils.SimpleTextWatcher
import com.kumagait.organizae.viewmodel.AccountViewmodel
import kotlinx.android.synthetic.main.activity_create_account.*
import org.jetbrains.anko.toast

class CreateAccountActivity : AppCompatActivity() {

    private val viewModel by viewModels<AccountViewmodel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)
        observeCreateAccountResponse()
        setupView()
    }

    private fun setupView() {
        btnBack.setOnClickListener { onBackPressed() }

        btnCreateAccount.setOnClickListener {
            viewModel.createAccount(etBalance.text.toString(), getDeviceSettings())
        }

        etBalance.run {
            addTextChangedListener(object : SimpleTextWatcher() {
                override fun afterTextChanged(s: Editable?) {
                    btnCreateAccount.isEnabled = s.toString().isNotBlank()
                }
            })
        }
    }

    private fun observeCreateAccountResponse() {
        viewModel.createAccountLiveData.observe(this, Observer { response ->
            when (response) {
                is StateSuccess -> {
                    toast("Conta criada com sucesso")
                    finish()
                }
                is StateError -> toast("Erro na aplicação")
            }
        })
    }

}