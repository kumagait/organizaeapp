package com.kumagait.organizae.view.ui.register

import android.os.Bundle
import android.text.TextWatcher
import android.text.method.KeyListener
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.kumagait.organizae.R
import com.kumagait.organizae.core.setStatusBarColor
import com.kumagait.organizae.service.model.UserRegisterBO
import kotlinx.android.synthetic.main.activity_base_register.*
import org.jetbrains.anko.textColor

abstract class BaseRegisterActivity : AppCompatActivity() {

    protected var userData: UserRegisterBO = UserRegisterBO()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColor(R.color.white_FFFFFF, true)
    }

    protected fun initRegisterViews(
        @StringRes tvLabel: Int,
        @StringRes etHint: Int,
        inputType: Int? = null,
        mask: TextWatcher? = null,
        textListener: TextWatcher? = null,
        buttonShouldStartEnabled: Boolean = false,
        keyListener: KeyListener? = null
    ) {
        btnBack.setOnClickListener {
            onBackPressed()
        }

        toggleGradientButton(buttonShouldStartEnabled)

        tvSignupLabel.text = getString(tvLabel)
        etSignup.run {
            hint = getString(etHint)
            inputType?.let { this.inputType = it }
            mask?.let { addTextChangedListener(it) }
            textListener?.let { addTextChangedListener(it) }
            keyListener?.let { this.keyListener = it }
        }
    }

    protected fun toggleGradientButton(isEnabled: Boolean) {
        btnRegisterNext.run {
            textColor = resources.getColor(
                if (isEnabled) R.color.blue_dark_252C3V
                else R.color.grey_dark_6B6B6B
            )
            this.isEnabled = isEnabled
        }
    }

    protected fun inputErrorVisibility(isVisible: Boolean, @StringRes text: Int? = null) {
        tvSignupError.run {
            visibility = if (isVisible) View.VISIBLE else View.INVISIBLE
            text?.let {
                this.text = getString(it)
            }
        }

    }

}