package com.kumagait.organizae.view.ui.register

import android.os.Bundle
import android.text.InputType
import android.text.method.DigitsKeyListener
import android.text.method.KeyListener
import com.kumagait.organizae.R
import com.kumagait.organizae.core.utils.EditTextMask
import com.kumagait.organizae.core.utils.SimpleTextWatcher
import kotlinx.android.synthetic.main.activity_base_register.*

class RegisterBirthDayActivity : BaseRegisterActivity(), RegisterRouter {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_register)
        userData = RegisterIntent.getUserData()
        initViews()
    }

    private fun initViews() {
        val nameValidator = object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                inputErrorVisibility(false)
                s?.let {
                    toggleGradientButton(it.length == BIRTHDAY_MASKED_LENGHT)
                }
            }
        }

        initRegisterViews(
            R.string.tv_label_birthday,
            R.string.et_hint_birthday,
            InputType.TYPE_CLASS_NUMBER,
            textListener = nameValidator,
            mask = EditTextMask.maskBirthday(),
            keyListener = DigitsKeyListener.getInstance("0123456789/")
        )

        btnRegisterNext.setOnClickListener {
            userData.birthdate = etSignup.text.toString()
            loadEmailActivity(userData)
        }
    }

    companion object {
        private const val BIRTHDAY_MASKED_LENGHT = 10
    }
}