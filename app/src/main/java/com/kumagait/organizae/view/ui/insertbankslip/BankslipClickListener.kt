package com.kumagait.organizae.view.ui.insertbankslip

import com.kumagait.organizae.service.model.BankslipDTO

interface BankslipClickListener {
    fun performClick(bankslipInfo: BankslipDTO)
}