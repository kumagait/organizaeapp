package com.kumagait.organizae.view.ui.account

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kumagait.organizae.R
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateSuccess
import com.kumagait.organizae.core.getDeviceSettings
import com.kumagait.organizae.core.utils.SessionUtil
import com.kumagait.organizae.service.model.AccountDTO
import com.kumagait.organizae.service.model.BankslipDTO
import com.kumagait.organizae.view.ui.bankslipdetail.BankslipDetailActivity
import com.kumagait.organizae.view.ui.bankslipdetail.BankslipDetailActivity.Companion.KEY_BANKSLIP_DETAIL
import com.kumagait.organizae.view.ui.home.HomeActivity
import com.kumagait.organizae.viewmodel.AccountViewmodel
import kotlinx.android.synthetic.main.activity_account.*
import kotlinx.android.synthetic.main.activity_account.btnBack
import kotlinx.android.synthetic.main.activity_account_details.*
import org.jetbrains.anko.*


class AccountActivity : AppCompatActivity(), AccountClickListener {

    private val viewModel by viewModels<AccountViewmodel>()
    private var bankslip: BankslipDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        observeAccountListResponse()
        observePaymentResponse()
        bankslip = intent.extras?.getParcelable(KEY_BANKSLIP_DETAIL)
        setupview()
    }

    private fun setupview() {
        btnBack.setOnClickListener {
            onBackPressed()
        }

        btnAddAccount.setOnClickListener {
            startActivity(intentFor<CreateAccountActivity>())
        }
    }

    override fun onResume() {
        super.onResume()
        if (bankslip == null) {
            viewModel.getAccountList(getDeviceSettings())
        } else {
            tvBankslipList.text = getString(R.string.tv_select_payment_account)
            btnAddAccount.visibility = View.GONE
            viewModel.getAvaliableAccountsForBankslip(bankslip?.bankslipId, getDeviceSettings())
        }
    }

    private fun observeAccountListResponse() {
        viewModel.accountListLiveData.observe(this, Observer { response ->
            when (response) {
                is StateSuccess -> {
                    if (response.data.accountList.isNotEmpty()) {
                        tvEmptyList.visibility = View.GONE
                        rvAccountList.run {
                            layoutManager = LinearLayoutManager(this@AccountActivity)
                            adapter = AccountListAdapter(
                                response.data.accountList,
                                this@AccountActivity
                            )
                        }
                    } else {
                        if (bankslip != null)
                            tvEmptyList.text = getString(R.string.tv_not_enough_balance)
                        else
                            tvEmptyList.text = getString(R.string.tv_no_accounts)
                        tvEmptyList.visibility = View.VISIBLE
                    }
                }
                is StateError -> {
                    toast("Erro na aplicação")
                }
            }
        })
    }

    private fun observePaymentResponse() {
        viewModel.paymentLiveData.observe(this, Observer { response ->
            when (response) {
                is StateSuccess -> {
                    toast("Pago com sucesso")
                    startActivity(intentFor<HomeActivity>().newTask().clearTask())
                }
                is StateError -> {
                    toast("Erro na aplicação")
                }
            }
        })
    }

    override fun onAccountClick(account: AccountDTO) {
        if (bankslip == null) {
            val bundle = Bundle().apply {
                putParcelable(KEY_ACCOUNT, account)
            }
            startActivity(Intent(this, AccountDetailActivity::class.java).putExtras(bundle))
        } else {
            showPaymentConfirmation(account)
        }
    }

    private fun showPaymentConfirmation(account: AccountDTO) {
        AlertDialog.Builder(this)
            .setTitle("Deseja pagar o boleto ${bankslip?.bankslipName}?")
            .setMessage("Ao concordar você pagará o boleto com o saldo da conta: ${account.accountName}")
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setPositiveButton(
                R.string.dialog_yes
            ) { _, _ ->
                viewModel.performPayment(account.accountId, bankslip?.bankslipId, getDeviceSettings())
            }
            .setNegativeButton(R.string.dialog_no, null).show()
    }

    companion object {
        const val KEY_ACCOUNT = "keyAccount"
    }
}