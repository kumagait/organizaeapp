package com.kumagait.organizae.view.ui.home

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kumagait.organizae.R
import com.kumagait.organizae.core.utils.formatToDate
import com.kumagait.organizae.service.model.BankslipDTO
import com.kumagait.organizae.service.model.BankslipStatus
import com.kumagait.organizae.service.model.BankslipStatus.*
import com.kumagait.organizae.view.ui.insertbankslip.BankslipClickListener
import kotlinx.android.synthetic.main.bankslip_view_item.view.*

class BankslipListAdapter(private val bankslipList: List<BankslipDTO>, private val listener: BankslipClickListener) :
    RecyclerView.Adapter<BankslipListAdapter.BankslipViewHolder>() {

    class BankslipViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): BankslipViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.bankslip_view_item, parent, false)
        return BankslipViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: BankslipViewHolder, position: Int) {
        holder.view.tvBankslipTitle.text = bankslipList[position].bankslipName
        holder.view.tvDueDate.text = bankslipList[position].dueDate.formatToDate()
        holder.view.tvBankslipValue.text = "R$ ${bankslipList[position].bankslipValue}".replace(".", ",")

        when(bankslipList[position].status) {
            PAID -> { holder.view.tvCloseBankslip.visibility = View.VISIBLE }
            OPEN -> { holder.view.tvOpenBankslip.visibility = View.VISIBLE }
        }

        holder.view.rootView.setOnClickListener {
            listener.performClick(bankslipList[position])
        }
    }

    override fun getItemCount() = bankslipList.size
}