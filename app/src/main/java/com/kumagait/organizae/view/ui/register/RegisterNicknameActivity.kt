package com.kumagait.organizae.view.ui.register

import android.os.Bundle
import android.text.InputType
import com.kumagait.organizae.R
import com.kumagait.organizae.core.utils.SimpleTextWatcher
import kotlinx.android.synthetic.main.activity_base_register.*

class RegisterNicknameActivity : BaseRegisterActivity(), RegisterRouter {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_register)
        userData = RegisterIntent.getUserData()
        initViews()
    }

    private fun initViews() {
        val nameValidator = object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let { toggleGradientButton(s.isNotEmpty()) }
            }
        }

        initRegisterViews(
            R.string.tv_label_nickname,
            R.string.et_hint_nickname,
            InputType.TYPE_TEXT_FLAG_CAP_WORDS,
            textListener = nameValidator
        )

        btnRegisterNext.setOnClickListener {
            userData.nickname = etSignup.text.toString()
            loadBirthdayActivity(userData)
        }
    }
}