package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class GetBankslipListResponse(
    @SerializedName("bankslip_list") val bankslipList: List<BankslipDTO>
) : Parcelable