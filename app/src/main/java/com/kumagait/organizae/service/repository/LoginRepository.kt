package com.kumagait.organizae.service.repository

import android.content.Context
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler

class LoginRepository : LoginService {

    fun logUser(
        context: Context,
        username: String,
        authHandler: AuthenticationHandler) {

        loginUser(context, username, authHandler)
    }
}