package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class TimelineResponse(
    @SerializedName("timeline") val timeline: List<TimelineItem>,
    @SerializedName("account_list") val accountList: List<String>
) : Parcelable

@Parcelize
class TimelineItem(
    @SerializedName("account_name") val accountName: String,
    @SerializedName("bankslip_name") val bankslipName: String?,
    @SerializedName("transaction_type") val transactionType: String,
    @SerializedName("transaction_value") val transactionValue: Float,
    @SerializedName("transaction_date") val transactionDate: String
) : Parcelable