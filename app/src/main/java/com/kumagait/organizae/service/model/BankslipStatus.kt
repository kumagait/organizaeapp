package com.kumagait.organizae.service.model

import com.google.gson.annotations.SerializedName

enum class BankslipStatus {
    @SerializedName("open") OPEN,
    @SerializedName("paid") PAID;
}