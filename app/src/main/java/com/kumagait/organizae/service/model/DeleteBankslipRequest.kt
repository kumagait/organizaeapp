package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class DeleteBankslipRequest(
    @SerializedName("user_id") val userId: String,
    @SerializedName("bankslip_id") val bankslipId: String
) : Parcelable