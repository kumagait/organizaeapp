package com.kumagait.organizae.service.repository

import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction
import com.kumagait.organizae.service.model.UserDTO

interface InsertUserService {
    @LambdaFunction
    fun insertUser(username: UserDTO): UserDTO

}