package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class CreateAccountRequestDTO(
    @SerializedName("username") val username: String,
    @SerializedName("account_name") val accountName: String,
    @SerializedName("device_info") val deviceInfo: String
) : Parcelable