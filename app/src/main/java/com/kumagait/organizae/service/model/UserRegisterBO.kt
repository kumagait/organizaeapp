package com.kumagait.organizae.service.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserRegisterBO(
    var birthdate: String = "",
    var email: String = "",
    var nickname: String = "",
    var username: String = "",
    var password: String = ""
) : Parcelable