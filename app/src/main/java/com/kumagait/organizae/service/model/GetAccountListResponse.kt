package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAccountListResponse(
    @SerializedName("account_list") val accountList: List<AccountDTO>
) : Parcelable

@Parcelize
data class AccountDTO(
    @SerializedName("account_id") val accountId: String,
    @SerializedName("account_balance") val accountBalance: Float,
    @SerializedName("account_name") val accountName: String
) : Parcelable