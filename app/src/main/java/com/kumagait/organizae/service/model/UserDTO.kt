package com.kumagait.organizae.service.model

import com.google.gson.annotations.SerializedName

data class UserDTO(
    @SerializedName("username") val username: String? = null,
    @SerializedName("client_id") val clientId: String? = null,
    @SerializedName("device_info") val deviceInfo: String? = null
)