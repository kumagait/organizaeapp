package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class GetBankslipListRequest(
    @SerializedName("user_id") val userId: String,
    @SerializedName("device_info") val deviceInfo: String
) : Parcelable