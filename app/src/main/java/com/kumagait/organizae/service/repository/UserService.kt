package com.kumagait.organizae.service.repository

import android.content.Context
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.UpdateAttributesHandler
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction
import com.kumagait.organizae.core.CognitoSettings
import com.kumagait.organizae.core.utils.SimpleAuthHandler
import com.kumagait.organizae.service.model.UserDTO
import java.lang.Exception

interface UserService {

    fun getUserDetails(context: Context, username: String, handler: GetDetailsHandler) {
        val user = CognitoSettings(context).getUserPool().getUser(username)
        user.getDetailsInBackground(handler)
    }

    fun deleteUser(context: Context, username: String, handler: GenericHandler) {
        val user = CognitoSettings(context).getUserPool().getUser(username)
        user.getSessionInBackground(object : SimpleAuthHandler() {
            override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
                user.deleteUserInBackground(handler)
                user.signOut()
            }
        })
    }

    fun updateInfo(context: Context, username: String, attributes: CognitoUserAttributes, handler: UpdateAttributesHandler) {
        val user = CognitoSettings(context).getUserPool().getUser(username)
        user.updateAttributesInBackground(attributes, handler)
    }
}