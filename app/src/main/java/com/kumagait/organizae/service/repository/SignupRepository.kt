package com.kumagait.organizae.service.repository

import android.content.Context
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.kumagait.organizae.service.model.UserRegisterBO
import io.reactivex.rxjava3.core.Single

class SignupRepository : SignupService {

    fun registerUser(
        userData: UserRegisterBO,
        context: Context,
        signUpHandler: SignUpHandler
    ) {

        val userAttributes = CognitoUserAttributes().apply {
            addAttribute("birthdate", userData.birthdate)
            addAttribute("email", userData.email)
            addAttribute("nickname", userData.nickname)
        }

        registerUser(userData, context, signUpHandler, userAttributes)
    }

    fun validateUserAccount(
        context: Context,
        confirmationCode: String,
        username: String,
        confirmationCallback: GenericHandler
    ) {
        validateUser(context, confirmationCode, username, confirmationCallback)
    }

}