package com.kumagait.organizae.service.repository

import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction
import com.kumagait.organizae.service.model.UserDTO

interface UserInfoService {
    @LambdaFunction
    fun deleteUser(userDTO: UserDTO?)
}