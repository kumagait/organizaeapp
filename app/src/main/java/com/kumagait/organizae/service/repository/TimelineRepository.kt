package com.kumagait.organizae.service.repository

import androidx.lifecycle.MutableLiveData
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.kumagait.organizae.core.StateResponse
import com.kumagait.organizae.core.utils.errorData
import com.kumagait.organizae.core.utils.successData
import com.kumagait.organizae.service.model.GetAccountListResponse
import com.kumagait.organizae.service.model.TimelineResponse
import com.kumagait.organizae.service.model.UserDTO
import org.json.JSONObject

class TimelineRepository : BaseRestRepository() {

    fun getTimeline(username: UserDTO, responseLiveData: MutableLiveData<StateResponse<TimelineResponse>>) {
        makeGetRequest("/getTimeline", username).getAsJSONObject(
            object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    responseLiveData.successData(mapResponse(response), true)
                }

                override fun onError(anError: ANError?) {
                    responseLiveData.errorData(Throwable(anError?.message), true)
                }
            }
        )
    }

}