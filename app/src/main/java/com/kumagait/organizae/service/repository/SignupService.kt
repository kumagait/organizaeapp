package com.kumagait.organizae.service.repository

import android.content.Context
import android.os.AsyncTask
import android.security.ConfirmationCallback
import androidx.constraintlayout.solver.widgets.ConstraintWidgetGroup
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.kumagait.organizae.core.CognitoSettings
import com.kumagait.organizae.service.model.UserRegisterBO

interface SignupService {

    fun registerUser(
        userData: UserRegisterBO,
        context: Context,
        signupCallback: SignUpHandler,
        userAttributes: CognitoUserAttributes
    ) {
        CognitoSettings(context).getUserPool().signUpInBackground(
            userData.username,
            userData.password,
            userAttributes,
            null,
            signupCallback
        )
    }

    fun validateUser(
        context: Context,
        confirmationCode: String,
        username: String,
        confirmationCallback: GenericHandler
    ) {
        AsyncTask.execute {
            val user = CognitoSettings(context).getUserPool().getUser(username)
            user.confirmSignUp(confirmationCode, false, confirmationCallback)
        }
    }

}