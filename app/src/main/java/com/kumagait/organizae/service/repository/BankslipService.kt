package com.kumagait.organizae.service.repository

import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction
import com.kumagait.organizae.service.model.BankslipDTO
import com.kumagait.organizae.service.model.DeleteBankslipRequest
import com.kumagait.organizae.service.model.GetBankslipListRequest
import com.kumagait.organizae.service.model.GetBankslipListResponse

interface BankslipService {

    @LambdaFunction
    fun insertBankslip(bankslipDTO: BankslipDTO?)

    @LambdaFunction
    fun getBankslipList(getBankslipDTO: GetBankslipListRequest?): GetBankslipListResponse

    @LambdaFunction
    fun removeBankslip(deleteBankslipRequest: DeleteBankslipRequest?)
}