package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class BankslipDTO(
    @SerializedName("bankslip_id") val bankslipId: String?=null,
    @SerializedName("bankslip_name") val bankslipName: String,
    @SerializedName("bankslip_code") val bankslipCode: String,
    @SerializedName("bankslip_value") val bankslipValue: Float,
    @SerializedName("status") val status: BankslipStatus,
    @SerializedName("due_date") val dueDate: String,
    @SerializedName("username") val username: String?=null,
    @SerializedName("device_info") val deviceInfo: String? = ""
) : Parcelable
