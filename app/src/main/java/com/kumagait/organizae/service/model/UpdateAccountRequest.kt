package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class UpdateAccountRequest(
    @SerializedName("account_id") val accountId: String,
    @SerializedName("action_value") val value: Float,
    @SerializedName("event_type") val eventType: String
) : Parcelable