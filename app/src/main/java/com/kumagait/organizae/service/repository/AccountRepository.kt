package com.kumagait.organizae.service.repository

import androidx.lifecycle.MutableLiveData
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.google.gson.Gson
import com.kumagait.organizae.core.API_KEY
import com.kumagait.organizae.core.BASE_URL
import com.kumagait.organizae.core.StateResponse
import com.kumagait.organizae.core.X_API_KEY
import com.kumagait.organizae.core.utils.error
import com.kumagait.organizae.core.utils.errorData
import com.kumagait.organizae.core.utils.success
import com.kumagait.organizae.core.utils.successData
import com.kumagait.organizae.service.model.*
import org.json.JSONObject
import kotlin.reflect.KClass

class AccountRepository : BaseRestRepository() {

    fun createAccount(request: CreateAccountRequestDTO, responseLiveData: MutableLiveData<StateResponse<*>>) {
            makePostRequest("/createAccount", request).getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    responseLiveData.success(response, true)
                }

                override fun onError(anError: ANError?) {
                    responseLiveData.error(Throwable(anError?.message), true)
                }
            })
    }

    fun performPayment(request: PerformPaymentRequest, responseLiveData: MutableLiveData<StateResponse<*>>) {
        makePostRequest("/performPayment", request).getAsJSONObject(object : JSONObjectRequestListener {
            override fun onResponse(response: JSONObject?) {
                responseLiveData.success(response, true)
            }

            override fun onError(anError: ANError?) {
                responseLiveData.error(Throwable(anError?.message), true)
            }
        })
    }

    fun updateAccount(request: UpdateAccountRequest, responseLiveData: MutableLiveData<StateResponse<*>>) {
        makePostRequest("/updateAccountBalance", request).getAsJSONObject(object : JSONObjectRequestListener {
            override fun onResponse(response: JSONObject?) {
                responseLiveData.success(response, true)
            }

            override fun onError(anError: ANError?) {
                responseLiveData.error(Throwable(anError?.message), true)
            }
        })
    }

    fun deleteAccount(request: DeleteAccountRequest, responseLiveData: MutableLiveData<StateResponse<GenericResponse>>) {
        makePostRequest("/removeAccount", request).getAsJSONObject(object : JSONObjectRequestListener {
            override fun onResponse(response: JSONObject?) {
                responseLiveData.successData(mapResponse(response), true)
            }

            override fun onError(anError: ANError?) {
                responseLiveData.errorData(Throwable(anError), true)
            }
        })
    }

    fun getAccountList(username: UserDTO, responseLiveData: MutableLiveData<StateResponse<GetAccountListResponse>>) {
        makeGetRequest("/getAccountList", username).getAsJSONObject(
            object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    responseLiveData.successData(mapResponse(response), true)
                }

                override fun onError(anError: ANError?) {
                    responseLiveData.errorData(Throwable(anError?.message), true)
                }
            }
        )
    }

    fun getAvaliableAccountsForBankslip(username: GetAvaliableAccountsRequest, responseLiveData: MutableLiveData<StateResponse<GetAccountListResponse>>) {
        makeGetRequest("/getAvaliableAccountsForPayment", username).getAsJSONObject(
            object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    responseLiveData.successData(mapResponse(response), true)
                }

                override fun onError(anError: ANError?) {
                    responseLiveData.errorData(Throwable(anError?.message), true)
                }
            }
        )
    }

}