package com.kumagait.organizae.service.repository

import android.content.Context
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction
import com.kumagait.organizae.core.CognitoSettings
import com.kumagait.organizae.service.model.UserDTO

interface LoginService {

    fun loginUser(context: Context, username: String, authHandler: AuthenticationHandler) {
        val user = CognitoSettings(context).getUserPool().getUser(username)
        user.getSessionInBackground(authHandler)
    }
}