package com.kumagait.organizae.service.repository

import android.content.Context
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.UpdateAttributesHandler
import com.kumagait.organizae.service.model.UserDTO

class UserRepository : UserService {

    fun getUserAttributes(
        context: Context,
        username: String,
        handler: GetDetailsHandler
    ) {
        getUserDetails(context, username, handler)
    }

    fun deleteAccount(
        context: Context,
        username: String,
        handler: GenericHandler
    ) {
        deleteUser(context, username, handler)
    }

    fun updateUserAttr(
        context: Context,
        username: String,
        attributes: CognitoUserAttributes,
        handler: UpdateAttributesHandler
    ) {
        updateInfo(context, username, attributes, handler)
    }

}