package com.kumagait.organizae.service.model

data class ValidationCallback(
    val isValidaCode: Boolean = false,
    val error: Exception? = null
)