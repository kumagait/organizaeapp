package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class PerformPaymentRequest(
    @SerializedName("account_id") val accountId: String,
    @SerializedName("bankslip_id") val bankslipId: String?,
    @SerializedName("device_info") val deviceInfo: String?
) : Parcelable