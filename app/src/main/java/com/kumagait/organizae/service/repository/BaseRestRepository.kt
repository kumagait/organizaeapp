package com.kumagait.organizae.service.repository

import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.google.gson.Gson
import com.kumagait.organizae.core.API_KEY
import com.kumagait.organizae.core.BASE_URL
import com.kumagait.organizae.core.X_API_KEY
import org.json.JSONObject

abstract class BaseRestRepository {
    fun <T> makePostRequest(path: String, request: T) =
        AndroidNetworking.post("$BASE_URL$path")
            .addJSONObjectBody(generateJsonObject(request))
            .addHeaders("Content-Type", "application/json")
            .addHeaders(X_API_KEY, API_KEY)
            .setPriority(Priority.MEDIUM)
            .doNotCacheResponse()
            .build()

    fun <T> makeGetRequest(path: String, request: T?) =
        AndroidNetworking.get("$BASE_URL$path")
            .addHeaders(request)
            .addHeaders("Content-Type", "application/json")
            .addHeaders(X_API_KEY, API_KEY)
            .setPriority(Priority.MEDIUM)
            .doNotCacheResponse()
            .build()

    fun <T> generateJsonObject(request: T): JSONObject = JSONObject(Gson().toJson(request))
    inline fun <reified T> mapResponse(response: JSONObject?): T =
        Gson().fromJson(response.toString(), T::class.java)
}