package com.kumagait.organizae.service.model

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails

data class SignupCallbackDTO(
    val state: SignupCallBackState,
    val user: CognitoUser?,
    val cognitoUserCodeDeliveryDetails: CognitoUserCodeDeliveryDetails?,
    val error: Exception?
)