package com.kumagait.organizae.service.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class GenericResponse(
    @SerializedName("statusCode") val statusCode: Int,
    @SerializedName("body") val body: String
) : Parcelable