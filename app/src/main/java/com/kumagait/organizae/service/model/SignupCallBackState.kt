package com.kumagait.organizae.service.model

enum class SignupCallBackState {
    SUCCESS_CONFIRMED,
    SUCCESS_NOT_CONFIRMED,
    ERROR
}