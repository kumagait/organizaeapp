package com.kumagait.organizae.core.utils

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import java.lang.Exception

open class SimpleAuthHandler: AuthenticationHandler {
    override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {}
    override fun onFailure(exception: Exception?) {}
    override fun getAuthenticationDetails(authenticationContinuation: AuthenticationContinuation?, userId: String?) {}
    override fun authenticationChallenge(continuation: ChallengeContinuation?) {}
    override fun getMFACode(continuation: MultiFactorAuthenticationContinuation?) {}
}