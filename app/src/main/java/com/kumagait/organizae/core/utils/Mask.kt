package com.kumagait.organizae.core.utils

class Mask(val mask: String) {
    private val charsFromMask: String

    init {
        val charsFromMask = StringBuilder()
        mask.forEach {
            if ('#' != it &&
                    !charsFromMask.contains(it))
                charsFromMask.append(it)
        }

        this.charsFromMask = charsFromMask.toString()
    }

    fun reformat(text: CharSequence): String {
        val formatted = StringBuilder()
        var maskOccurance = 0
        val unsmaskText = removeMask(
            if (text.length > mask.length) {
                text.subSequence(0, mask.length)
            } else text
        )

        unsmaskText.forEachIndexed { index, char ->
            val indexMask = index + maskOccurance

            when {
                indexMask >= mask.length -> return@forEachIndexed
                '#' == mask[indexMask] -> formatted.append(char)
                else -> {
                    maskOccurance += addMask(formatted, indexMask)
                    formatted.append(char)
                }
            }
        }

        return formatted.toString()
    }

    private fun addMask(formatted: StringBuilder, indexMask: Int): Int {
        var maskOccurence = 0
        for (index in indexMask until mask.length) {
            val maskChar = mask[index]
            if ('#' != maskChar) {
                formatted.append(maskChar)
                maskOccurence++
            } else return maskOccurence
        }

        return maskOccurence
    }

    private fun removeMask(text: CharSequence): String {
        return text.replace("[$charsFromMask]".toRegex(), "")
    }
}