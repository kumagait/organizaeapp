package com.kumagait.organizae.core.utils

import androidx.lifecycle.MutableLiveData
import com.kumagait.organizae.core.StateError
import com.kumagait.organizae.core.StateResponse
import com.kumagait.organizae.core.StateSuccess

fun <T> MutableLiveData<StateResponse<*>>.success(data: T, isUserFreeToChangeScreen: Boolean = false) =
    if (isUserFreeToChangeScreen) {
        postValue(StateSuccess(data))
    } else {
        value = StateSuccess(data)
    }

fun MutableLiveData<StateResponse<*>>.error(error: Throwable, isUserFreeToChangeScreen: Boolean = false) =
    if (isUserFreeToChangeScreen) {
        postValue(StateError<Throwable>(error))
    } else {
        value = StateError<Throwable>(error)
    }

fun <T> MutableLiveData<StateResponse<T>>.successData(data: T, isUserFreeToChangeScreen: Boolean = false) =
    if (isUserFreeToChangeScreen) {
        postValue(StateSuccess(data))
    } else {
        value = StateSuccess(data)
    }

fun <T> MutableLiveData<StateResponse<T>>.errorData(error: Throwable, isUserFreeToChangeScreen: Boolean = false) =
    if (isUserFreeToChangeScreen) {
        postValue(StateError(error))
    } else {
        value = StateError(error)
    }