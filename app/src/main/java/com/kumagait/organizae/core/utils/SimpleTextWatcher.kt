package com.kumagait.organizae.core.utils

import android.text.Editable
import android.text.TextWatcher

open class SimpleTextWatcher : TextWatcher {
    override fun afterTextChanged(s: Editable?) {/* implementation abstracted */}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {/* implementation abstracted */}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {/* implementation abstracted */}

}