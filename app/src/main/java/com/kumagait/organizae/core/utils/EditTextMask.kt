package com.kumagait.organizae.core.utils

import android.text.Editable

object EditTextMask {

    private const val BIRTHDAY_MASK = "##/##/####"

    fun maskBirthday() = mask(BIRTHDAY_MASK)

    private fun mask(mask: String): SimpleTextWatcher {
        return object : SimpleTextWatcher() {
            private var selfChange = false
            private val mask = Mask(mask)

            override fun afterTextChanged(s: Editable?) {
                if(selfChange)
                    return

                selfChange = true
                val formatted = this.mask.reformat(s.toString())
                if (formatted != s.toString())
                    s!!.replace(0, s.length, formatted, 0, formatted.length)
                selfChange = false
            }

        }
    }

}