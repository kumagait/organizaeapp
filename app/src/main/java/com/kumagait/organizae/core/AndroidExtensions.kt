package com.kumagait.organizae.core

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Build
import android.provider.Settings
import android.view.View
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.kumagait.organizae.core.utils.SessionUtil

fun Activity.setStatusBarColor(@ColorRes idColor: Int, hasLightTextColor: Boolean = false) {
    window.statusBarColor = getResColor(idColor)
    if (hasLightTextColor)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
}

fun Context.getResColor(@ColorRes idColor: Int) = ContextCompat.getColor(this, idColor)

@SuppressLint("HardwareIds")
fun Activity.getDeviceSettings(): String {
    val cognitoDeviceKey = SessionUtil.deviceKey
    val id = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
    val brand = Build.BRAND
    val model = Build.MODEL
    val device = Build.DEVICE
    val userSub = SessionUtil.clientSub

    return "USER_SUB: $userSub - DEVICE_ID: $id - COGNITO_DEVICE_KEY: $cognitoDeviceKey - BRAND: $brand - MODEL: $model - DEVICE: $device"
}