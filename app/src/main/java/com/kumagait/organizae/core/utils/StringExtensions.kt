package com.kumagait.organizae.core.utils

fun String.formatToDate() = "${takeLast(2)}/${substring(5, 7)}/${take(4)}"