package com.kumagait.organizae.core

import android.content.Context
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import com.amazonaws.regions.Regions

class CognitoSettings(private val context: Context) {

    private var userPoolId = "us-east-1_FpEqqJGJH"
    private var clientId = "2s59o0ihvhjon8nelvtlra0nh7"
    private var clientSecret = "1bu12mp14arnt815vla7gh42nfa1648lpksh0favu6efbe6e00m9"
    private val region = Regions.US_EAST_1

    fun getUserPoolId() = userPoolId
    fun getClientId() = clientId
    fun getClientSecret() = clientSecret

    fun getUserPool() =
        CognitoUserPool(
            context,
            userPoolId,
            clientId,
            clientSecret,
            region
        )
}