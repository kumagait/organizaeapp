package com.kumagait.organizae.core.utils

import android.app.Application
import com.androidnetworking.AndroidNetworking

class AppApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidNetworking.initialize(applicationContext)
    }
}