package com.kumagait.organizae

import android.content.Intent
import android.os.Build
import android.widget.Button
import com.kumagait.organizae.view.ui.login.LoginActivity
import com.kumagait.organizae.view.ui.MainActivity
import com.kumagait.organizae.view.ui.register.RegisterNicknameActivity
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric.buildActivity
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P])
class MainActivityTest {

    @Test
    fun `when click on login should go to LoginActivity`() {
        val controller = buildActivity(MainActivity::class.java).setup()
        val activity = controller.get()

        activity.findViewById<Button>(R.id.btnLogin).performClick()

        val expectedIntent = Intent(activity, LoginActivity::class.java)
        val actual = shadowOf(RuntimeEnvironment.application).nextStartedActivity

        assertEquals(expectedIntent.component, actual.component)
    }

    @Test
    fun `when click on singup should go to RegisterNicknameActivity`() {
        val controller = buildActivity(MainActivity::class.java).setup()
        val activity = controller.get()

        activity.findViewById<Button>(R.id.btnRegister).performClick()

        val expectedIntent = Intent(activity, RegisterNicknameActivity::class.java)
        val actual = shadowOf(RuntimeEnvironment.application).nextStartedActivity

        assertEquals(expectedIntent.component, actual.component)
    }

}